<?php

namespace Kunli00\Gif\Console\Commands;

use Illuminate\Console\Command;

class Install extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'template:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Template for application database migrations.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Package install');

        // Running any needed migrations
        $this->call('migrate');

        return 1;
    }
}
