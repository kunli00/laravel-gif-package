<?php

namespace Kunli00\Gif;

use Illuminate\Support\ServiceProvider;
use Kunli00\Gif\Console\Commands\Install;

class GifServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/resources/views','gif');

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->mergeConfigFrom(__DIR__ . '/config/gif.php','gif');

        $this->publishes([
            __DIR__ . '/config/gif.php' => config_path('gif.php'),
        ], 'config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                Install::class,
            ]);
        }
    }


    public function register()
    {
        parent::register(); // TODO: Change the autogenerated stub
    }
}
