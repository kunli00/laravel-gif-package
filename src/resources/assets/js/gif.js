function weather() {
    var apiKey = "2b58e0acaa0f90646b8061ce69f42e80";
    var url = "https://api.forecast.io/forecast/";

    latitude = 27.6671339;
    longitude = -80.4278155;

    $.getJSON(
        url + apiKey + "/" + latitude + "," + longitude + "?callback=?",
        function(data) {
            $("#temp").html(data.currently.temperature + "° F");
            $("#minutely").html(data.minutely.summary);
        }
    );
}
weather();
