<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Template</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('gif/css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content">
        <div>
            <video >
                <source src="{{ asset('gif/celebrate.mp4') }}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
        <div class="title m-b-md">
           Gif
        </div>

        <div class="links">

            <a href="https://laravel.com/docs/5.8/providers" target="_blank">Service Providers</a>
            <a href="https://laravel.com/docs/5.8/packages#resources" target="_blank">Resources</a>
        </div>
        <div id="weather">
            <div id="temp"></div><div id="minutely"></div>
            <div id="location"></div>
        </div>


    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('gif/js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.simpleWeather/3.1.0/jquery.simpleWeather.min.js"></script>
</body>
</html>
