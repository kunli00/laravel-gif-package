<?php

Route::group(['namespace' => 'Kunli00\Gif\Http\Controllers'], function(){

    Route::get('gif', 'GifController@index');

    Route::get('gif/css', 'GifController@css');

    Route::get('gif/js', 'GifController@js');
});
