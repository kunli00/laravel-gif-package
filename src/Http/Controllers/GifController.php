<?php

namespace Kunli00\Gif\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

class GifController extends Controller
{
    public function index(){
        return view('gif::gif');
    }

    public function css(){

        if (App::environment('local')){
            $path = base_path('package/gif/src/resources/assets/css/gif.css');
        }
        else{
            $path = base_path('vendor/kun/gif/src/resources/assets/css/gif.css');
        }

        $css = file_get_contents($path);

        $response = new Response( $css, 200, ['Content-Type' => 'text/css']);

        return $response;
    }

    public function js(){

        if (App::environment('local')){
            $path = base_path('package/gif/src/resources/assets/js/gif.js');
        }
        else{
            $path = base_path('vendor/kun/gif/src/resources/assets/js/gif.js');
        }

        $js = file_get_contents($path);

        $response = new Response( $js, 200, ['Content-Type' => 'text/javascript']);

        return $response;
    }
}
